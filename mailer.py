import smtplib
from email.mime.text import MIMEText


class Mailer:
    """ Класс для отправки сообщений
    """
    def __init__(self, server, port, login, password):
        address = "%s:%s" % (server, port)
        self.server = smtplib.SMTP_SSL(server, port)
        self.server.ehlo()
        self.server.login(login, password)
        self.login = login

    def send(self, recipient, text):
        message = MIMEText(text)
        message['Subject'] = "Тестовое задание от Евгения Усова"
        message['From'] = self.login
        message['To'] = recipient

        self.server.sendmail(self.login, recipient, message.as_string())
        self.server.close()