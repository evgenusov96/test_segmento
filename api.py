from pypodio2 import api
import settings


class API:
    """ Класс обёртка для работы с апи подио
    """
    def __init__(self, client_id, cliend_secret, username, password):
        self.api = api.OAuthClient(client_id, cliend_secret, username, password)

    def get_mediaplans(self, attributes):
        return self.api.Item.filter(settings.MEDIAPLANS_ID, attributes=attributes)

    def get_compaigns(self, attributes):
        return self.api.Item.filter(settings.COMPAIGNS_ID, attributes=attributes)

    def find(self, id):
        return self.api.Item.find(id)
