import time

import settings
import utils
from api import API


api = API(
    settings.CLIEND_ID,
    settings.CLIENT_SECRET,
    settings.USERNAME,
    settings.PASSWORD
)


def load_mediplans():
    """
    Выгружаем все медиапланы по фильтру
    :return: 
    """
    r = api.get_mediaplans({
        'filters': {
            settings.FIELDS['mediplans']['status']: [
                settings.VALUES['mediaplans']['status']['confirmed']
            ]
        },
        'limit': 126
    })

    return r['items']


def load_compaigns():
    """
    Выгружаем все кампании
    :return: 
    """
    r = api.get_compaigns({
        'filters': {
            settings.FIELDS['compaigns']['status']: settings.VALUES['compaigns']['status']['active'],
            settings.FIELDS['compaigns']['category']: settings.VALUES['compaigns']['category']['desktop']
        },
        'limit': 200
    })

    return r['items']


def calc_account_manager_mp(mediaplans):
    """
    Считаем медиапланы по аккаунт-менеджеру
    :param mediaplans: 
    :return: 
    """
    account_managers = {}

    for mediaplan in mediaplans:
        fields = utils.unpack_fields(mediaplan['fields'])
        account_manager_html = utils.clean_html(fields['Account Manager']['values'][0]['value'])
        account_manager = utils.clean_html(account_manager_html)  # Костыль для того, чтобы урезать html теги

        if account_manager not in account_managers:
            account_managers[account_manager] = 0

        account_managers[account_manager] += 1

    return account_managers


def calc_mp_with_compaigns(compaigns):
    """
    Считаем все медиапланы с кампаниями
    :param compaigns: 
    :return: 
    """
    all = []
    converted = []

    for compaign in compaigns:
        compaign_fields = utils.unpack_fields(compaign['fields'])
        mediaplans = compaign_fields['Mediaplan']['values']

        for mediaplan in mediaplans:
            item_id = mediaplan['value']['item_id']
            mediaplan = api.find(item_id)
            mediaplan_fields = utils.unpack_fields(mediaplan['fields'])
            status = mediaplan_fields['Status']['values'][0]['value']['text']

            if status == 'Confirmed':
                all.append(mediaplan)
                for status in compaign_fields['Status']['values']:
                    if status['value']['text'] == "Active":
                        converted.append(mediaplan)

    return {
        'with_compaigns': all,
        'converted': converted
    }



def main():
    mediaplans = load_mediplans()
    compaigns = load_compaigns()
    account_managers = calc_account_manager_mp(mediaplans)
    mp_with_compaigns = calc_mp_with_compaigns(compaigns)
    message = utils.generate_message(account_managers, mediaplans, mp_with_compaigns['converted'])

    utils.send_mail(settings.RECIPIENT, message)


if __name__ == '__main__':
    while True:
        main()
        time.sleep(settings.INTERVAL_CHECK)
