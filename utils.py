import re

import settings
from mailer import Mailer


def clean_html(str):
    return re.sub('<[^<]+?>', '', str)


def unpack_fields(fields):
    pack = {}

    for field in fields:
        pack[field['label']] = field

    return pack


def generate_message(account_managers, all_mediplans, with_compaing):
    """
    Функция для генерации тела письма
    :param account_managers: 
    :param all_mediplans: 
    :param with_compaing: 
    :return: 
    """
    part1 = ""

    for account_manager, count in account_managers.items():
        str = "%s:%s;\n" %(account_manager, count)
        part1 += str

    part2 = len(with_compaing)

    part3 = "%s:%s" % (part2, len(all_mediplans))

    template = """
        1. Всего медиапланов по аккаунт менеджерам:\n
        %s
        \n
        2. С привязкой к кампаниям: %s\n
        3. В соотношении %s\n
        
        _____
        by Евгений Усов
    """ % (part1, part2, part3)


    return template


def send_mail(to, text):
    """
    Функция для отправки письма
    :param to: 
    :param text: 
    :return: 
    """
    mailer = Mailer(settings.MAIL['server'],
                    settings.MAIL['port'],
                    settings.MAIL['login'],
                    settings.MAIL['password'])

    mailer.send(to, text)